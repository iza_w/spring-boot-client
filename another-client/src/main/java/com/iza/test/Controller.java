package com.iza.test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class Controller {

    private Token token;

    @Autowired
    private OAuth2RestOperations oAuth2RestOperations;

    @GetMapping(value = "/raw")
    public String raw() {
        if (token == null) {
            token = getToken();
        }
        return postCreateOrder_restTemplate(token.getAccess_token());
    }

    @GetMapping(value = "/oauth2")
    public String oauth2() {
        OAuth2AccessToken accessToken = oAuth2RestOperations.getAccessToken();
        return postCreateOrder_restTemplate(accessToken.getValue());
    }

    private Token getToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        RestTemplate restTemplate = new RestTemplate();

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "password");
        map.add("username", "user");
        map.add("password", "user");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        restTemplate.getInterceptors().add(
                new BasicAuthorizationInterceptor("my-trusted-client", "secret"));
        ResponseEntity<String> response = restTemplate
                .postForEntity("http://localhost:8080/oauth/token", request, String.class, map);

        Gson gson = new GsonBuilder().create();
        return gson.fromJson(response.getBody(), Token.class);
    }

    private String postCreateOrder_restTemplate(String token) {
        String url = "http://localhost:8080/private";

        RestTemplate restTemplate = new RestTemplate();

//        HttpHeaders headers = new HttpHeaders();
//        headers.set("Authorization", String.format("%s %s", "bearer", token));
//        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("access_token", token);

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url).queryParam("access_token", token);

        ResponseEntity<String> response = restTemplate
                .getForEntity(uriBuilder.toUriString(), String.class, map);

        return response.getBody();
    }
}
